from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json
import pandas as pd

url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
parameters = {
    'id': '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15',
    'convert': 'USD'
}
headers = {
    'Accepts': 'application/json',
    'X-CMC_PRO_API_KEY': '517e869a-83db-432a-855d-bb7d3291c235',
}

session = Session()
session.headers.update(headers)

try:
    response = session.get(url, params=parameters)
    data = json.loads(response.text)
    print(data)
    with open("data.json", "w") as write_file:
        json.dump(data, write_file)

except (ConnectionError, Timeout, TooManyRedirects) as e:
    print(e)

df = pd.read_json(r'data.json')
df.to_csv(r'data.csv', index=None)
