#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
okay so this works but the issue is how to have the function or other script know that if I send yes or no in
plain text they can call a function with that yes or no and that sucks. if I encrypt it they can just see the
hardcoded password. :C thought needed
"""
import tkinter as tk
import argon2
import pickle
import atexit

window = tk.Tk()
window.title('My Window')


def exit_handler():
    save_obj(UserHashDict, "UsernameAndPassword")


atexit.register(exit_handler)


def checkPass():
    global userHash
    username = e1.get()
    password = e2.get()
    ph = argon2.PasswordHasher()
    try:
        userHash = UserHashDict[username]
    except:
        print("Bad Username")
    try:
        ph.verify(userHash, password)
        print("Good Password")
    except:
        print("Bad Password")


def save_obj(obj, name):
    with open('obj/' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


UserHashDict = load_obj("UsernameAndPassword")

print(UserHashDict)

UserHashDict = {
    "myUsername": "$argon2id$v=19$m=102400,t=2,p=8$HTg1atL8z4dpsbmhHn1Nwg$znHfCx2J0fQD0mFdlxhksA",
}

l1 = tk.Label(text="Username")
l1.pack()
e1 = tk.Entry(window, show=None, font=('Arial', 14))
e2 = tk.Entry(window, show='*', font=('Arial', 14))
e1.pack(side=tk.TOP, anchor=tk.E)
l2 = tk.Label(text="Password")
l2.pack()
e2.pack(anchor=tk.E)
b1 = tk.Button(text="Submit", command=lambda: checkPass())
b1.pack()

window.mainloop()
