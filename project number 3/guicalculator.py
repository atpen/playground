import tkinter as tk
import tkinter.font as font
from sympy import simplify


def addZero():
    ent_box.insert(tk.END, "0")


def addTwo():
    ent_box.insert(tk.END, "2")


def addPeriod():
    ent_box.insert(tk.END, ".")


def addOne():
    ent_box.insert(tk.END, "1")


def addThree():
    ent_box.insert(tk.END, "3")


def addFour():
    ent_box.insert(tk.END, "4")


def addFive():
    ent_box.insert(tk.END, "5")


def addSix():
    ent_box.insert(tk.END, "6")


def addSeven():
    ent_box.insert(tk.END, "7")


def addEight():
    ent_box.insert(tk.END, "8")


def addNine():
    ent_box.insert(tk.END, "9")


def addPlus():
    ent_box.insert(tk.END, "+")


def addMinus():
    ent_box.insert(tk.END, "-")


def addMultiply():
    ent_box.insert(tk.END, "*")


def addDivide():
    ent_box.insert(tk.END, "/")


def deleteLast():
    current = ent_box.get()
    current = current[:-1]
    ent_box.delete(0, tk.END)
    ent_box.insert(tk.END, current)


def Clear():
    ent_box.delete(0, tk.END)


def equal(event):
    expression = ent_box.get()
    print(event)
    print(simplify(expression))
    ent_box.delete(0, tk.END)
    ent_box.insert(0, simplify(expression))


def equal1():
    expression = ent_box.get()
    print(simplify(expression))
    ent_box.delete(0, tk.END)
    ent_box.insert(0, simplify(expression))


window = tk.Tk()
window.title('Calculator')
myFont = font.Font(size=30)
window.rowconfigure([0, 1, 2, 3, 4], minsize=50, weight=1)
window.columnconfigure([0, 1, 2, 3], minsize=50, weight=1)

ent_box = tk.Entry(master=window, font=myFont)
ent_box.grid(row=0, column=3, sticky="nsew")

btn_zero = tk.Button(master=window, text="0", command=addZero, font=myFont)
btn_zero.grid(row=4, column=0, sticky="nsew")

btn_one = tk.Button(master=window, text="1", command=addOne, font=myFont)
btn_one.grid(row=4, column=1, sticky="nsew")

btn_two = tk.Button(master=window, text="2", command=addTwo, font=myFont)
btn_two.grid(row=4, column=2, sticky="nsew")

btn_three = tk.Button(master=window, text="3", command=addThree, font=myFont)
btn_three.grid(row=3, column=0, sticky="nsew")

btn_four = tk.Button(master=window, text="4", command=addFour, font=myFont)
btn_four.grid(row=3, column=1, sticky="nsew")

btn_five = tk.Button(master=window, text="5", command=addFive, font=myFont)
btn_five.grid(row=3, column=2, sticky="nsew")

btn_six = tk.Button(master=window, text="6", command=addSix, font=myFont)
btn_six.grid(row=2, column=0, sticky="nsew")

btn_seven = tk.Button(master=window, text="7", command=addSeven, font=myFont)
btn_seven.grid(row=2, column=1, sticky="nsew")

btn_eight = tk.Button(master=window, text="8", command=addEight, font=myFont)
btn_eight.grid(row=2, column=2, sticky="nsew")

btn_nine = tk.Button(master=window, text="9", command=addNine, font=myFont)
btn_nine.grid(row=1, column=0, sticky="nsew")

btn_period = tk.Button(master=window, text=".", command=addPeriod, font=myFont)
btn_period.grid(row=1, column=1, sticky="nsew")

btn_Plus = tk.Button(master=window, text="+", command=addPlus, font=myFont)
btn_Plus.grid(row=4, column=3, sticky="nsew")

btn_Minus = tk.Button(master=window, text="-", command=addMinus, font=myFont)
btn_Minus.grid(row=3, column=3, sticky="nsew")

btn_Multiply = tk.Button(master=window, text="*", command=addMultiply, font=myFont)
btn_Multiply.grid(row=2, column=3, sticky="nsew")

btn_Divide = tk.Button(master=window, text="/", command=addDivide, font=myFont)
btn_Divide.grid(row=1, column=3, sticky="nsew")

btn_Equal = tk.Button(master=window, text="=", command=equal1, font=myFont)
btn_Equal.grid(row=1, column=2, sticky="nsew")

btn_Delete = tk.Button(master=window, text="Del", command=deleteLast, font=myFont)
btn_Delete.grid(row=0, column=1, sticky="nsew")

btn_Clear = tk.Button(master=window, text="CE", command=Clear, font=myFont)
btn_Clear.grid(row=0, column=2, sticky="nsew")

window.bind("<Return>", equal)

window.mainloop()
