# Import the modules
import turtle
import random
import screen

# Create a new turtle.
turtle.colormode(255)
angle = float(input("What angle do you want? "))
distance = int(input("What distance do you want? "))
modifier = float(input("What modifier do you want? "))
speed = int(input("What speed do you want? (0 normal, 1 fast, 2 fastest) "))
if speed == 1 or speed == 2:
        turtle.speed("fastest")
if speed == 2:
        turtle.tracer(n=500, delay=0)
IAmATurtle = turtle.Turtle()
for i in range(0, int(input("how many layers? "))):
        IAmATurtle.pencolor(
                random.randint(1, 255), random.randint(1, 255), random.randint(1, 255))
        IAmATurtle.forward(distance * int(i) * modifier)
        IAmATurtle.right(angle)
# Stop turtle
turtle.update()
input("Press Enter to continue...")
turtle.done()
